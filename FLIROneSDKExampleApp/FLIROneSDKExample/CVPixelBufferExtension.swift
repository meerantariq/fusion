//
//  CVPixelBufferExtension.swift
//  FLIROneSDKExample
//
//  Created by Meeran Tariq on 08/02/2019.
//  Copyright © 2019 novacoast. All rights reserved.
//

import AVFoundation
import UIKit

class CustomBuffer: NSObject {
    
    override init() {
        
    }
    
    @objc public func clamp(_ buffer: CVPixelBuffer) -> CVPixelBuffer {
        
        let width = CVPixelBufferGetWidth(buffer)
        let height = CVPixelBufferGetHeight(buffer)
        
        CVPixelBufferLockBaseAddress(buffer, CVPixelBufferLockFlags(rawValue: 0))
        let floatBuffer = unsafeBitCast(CVPixelBufferGetBaseAddress(buffer), to: UnsafeMutablePointer<Float>.self)
        
        for y in 0 ..< height {
            for x in 0 ..< width {
                let pixel = floatBuffer[y * width + x]
                floatBuffer[y * width + x] = min(1.0, max(pixel, 0.0))
            }
        }
        
        CVPixelBufferUnlockBaseAddress(buffer, CVPixelBufferLockFlags(rawValue: 0))
        return buffer
    }
}
