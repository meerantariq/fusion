//
//  FLIROneSDKEditorViewController.h
//  FLIROneSDKExampleApp
//
//  Created by Colicchio, Joseph on 6/22/17.
//  Copyright © 2017 novacoast. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FLIROneSDKEditorViewController : UIViewController

@property (strong, nonatomic) NSURL *filepath;

@end
