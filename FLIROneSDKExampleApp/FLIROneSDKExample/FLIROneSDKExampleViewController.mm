//
//  FLIROneSDKExampleViewController.m
//  FLIROneSDKExample
//
//  Created by Joseph Colicchio on 5/22/14.
//  Copyright (c) 2014 novacoast. All rights reserved.
//



#import "FLIROneSDKExampleViewController.h"
#import "FLIROneSDKEditorViewController.h"

//#import <FLIROneSDK/FLIROneSDKLibraryViewController.h>

#import <AVFoundation/AVFoundation.h>

#import <FLIROneSDK/FLIROneSDKUIImage.h>

#import <FLIROneSDK/FLIROneSDKSimulation.h>

#import <FLIROneSDK/FLIROneSDKSimulation.h>

#import <DangerObjectDetect/DangerObjectsDetector.h>

#import "AAPLPreviewView.h"

#import <AssetsLibrary/AssetsLibrary.h>

#import "FLIROneSDKExampleApp-Swift.h"

#import <Photos/Photos.h>

#import <sys/utsname.h>


static void * CapturingStillImageContext = &CapturingStillImageContext;
static void * SessionRunningContext = &SessionRunningContext;
typedef NS_ENUM( NSInteger, AVCamSetupResult ) {
    AVCamSetupResultSuccess,
    AVCamSetupResultCameraNotAuthorized,
    AVCamSetupResultSessionConfigurationFailed
};


@interface FLIROneSDKExampleViewController () <AVCaptureVideoDataOutputSampleBufferDelegate, AVCaptureDepthDataOutputDelegate>

@property AVCaptureDevice *videoDevice;
@property AVCaptureSession *captureSession;
@property dispatch_queue_t captureSessionQueue;


// For use in the storyboards.
@property (nonatomic, weak) IBOutlet AVCaptureVideoPreviewLayer *previewView;
@property (nonatomic, weak) IBOutlet UILabel *cameraUnavailableLabel;
@property (nonatomic, weak) IBOutlet UIButton *resumeButton;
@property (nonatomic, weak) IBOutlet UIButton *recordButton;
@property (nonatomic, weak) IBOutlet UIButton *cameraButton;
@property (nonatomic, weak) IBOutlet UIButton *stillButton;

// For threshold management
@property (weak, nonatomic) IBOutlet UISwitch *thresholdSwitch;

// For algorithm management
@property (weak, nonatomic) IBOutlet UISwitch *algorithmSwitch;


// Session management.
@property (nonatomic) dispatch_queue_t sessionQueue;
@property (nonatomic) AVCaptureSession *session;
@property (nonatomic) AVCaptureDeviceInput *videoDeviceInput;
@property (nonatomic) AVCaptureMovieFileOutput *movieFileOutput;
@property (nonatomic) AVCaptureStillImageOutput *stillImageOutput;

// Utilities.
@property (nonatomic) AVCamSetupResult setupResult;
@property (nonatomic, getter=isSessionRunning) BOOL sessionRunning;
@property (nonatomic) UIBackgroundTaskIdentifier backgroundRecordingID;


//The main viewfinder for the FLIR ONE
@property (weak, nonatomic) IBOutlet UIView *masterImageView;
@property (strong, nonatomic) IBOutlet UIImageView *thermalImageView;
@property (strong, nonatomic) IBOutlet UIImageView *radiometricImageView;
@property (strong, nonatomic) IBOutlet UIImageView *visualJPEGView;
@property (strong, nonatomic) IBOutlet UIImageView *visualYCbCrView;
@property (weak, nonatomic) IBOutlet UIImageView *depthMaskView;

@property (strong, nonatomic) IBOutlet UIButton *thermalButton;
@property (strong, nonatomic) IBOutlet UIButton *thermal14BitButton;
@property (strong, nonatomic) IBOutlet UIButton *visualJPEGButton;
@property (strong, nonatomic) IBOutlet UIButton *visualYCbCrButton;

//labels outlining various camera information
@property (strong, nonatomic) IBOutlet UILabel *connectionLabel;
@property (strong, nonatomic) IBOutlet UILabel *tuningStateLabel;
@property (strong, nonatomic) IBOutlet UILabel *versionLabel;
@property (strong, nonatomic) IBOutlet UILabel *batteryChargingLabel;
@property (strong, nonatomic) IBOutlet UILabel *batteryPercentageLabel;

@property (strong, nonatomic) IBOutlet UILabel *frameCountLabel;

@property (strong, nonatomic) IBOutlet UIButton *paletteButton;

@property (strong, nonatomic) IBOutlet UIButton *emissivityButton;
@property (strong, nonatomic) IBOutlet UIButton *msxButton;
@property (strong, nonatomic) IBOutlet UIButton *tuneButton;
@property (strong, nonatomic) IBOutlet UISwitch *autoTuneSwitch;
@property (weak, nonatomic) IBOutlet UIButton *spanLockButton;

@property (weak, nonatomic) IBOutlet UILabel *timerLabel;


@property (strong, nonatomic) UIView *regionView;
@property (strong, nonatomic) UILabel *regionMinLabel;
@property (strong, nonatomic) UILabel *regionMaxLabel;
@property (strong, nonatomic) UILabel *regionAverageLabel;

@property (strong, nonatomic) UIView *hottestPoint;
@property (strong, nonatomic) UILabel *hottestLabel;
@property (strong, nonatomic) UIView *coldestPoint;
@property (strong, nonatomic) UILabel *coldestLabel;

@property (strong, nonatomic) NSData *thermalData;
@property (nonatomic) CGSize thermalSize;
@property (nonatomic) BOOL thermalDataIsFloats;

//buttons for interacting with the FLIR ONE
//view library
@property (nonatomic, strong) IBOutlet UIButton *editButton;
//capture photo
@property (nonatomic, strong) IBOutlet UIButton *capturePhotoButton;
//capture video
@property (nonatomic, strong) IBOutlet UIButton *captureVideoButton;
//swap palettes, button overlays the viewfinder
//@property (nonatomic, strong) UIButton *imageButton;
@property (weak, nonatomic) IBOutlet UIButton *vividIRButton;

//data for UI to display
@property (strong, nonatomic) UIImage *thermalImage;
@property (strong, nonatomic) UIImage *radiometricImage;
@property (strong, nonatomic) UIImage *visualJPEGImage;
@property (strong, nonatomic) UIImage *visualYCbCrImage;

//@property (strong, nonatomic) FLIROneSDKUIImage *sdkImage;

@property (strong, nonatomic) NSDictionary *spotTemperatures;
@property (strong, nonatomic) FLIROneSDKPalette *palette;
@property (nonatomic) NSUInteger paletteCount;

@property (nonatomic) BOOL connected;
@property (nonatomic) BOOL isDongle;
@property (nonatomic) BOOL isGen3Dongle;
@property (nonatomic) BOOL isProDongle;

@property (nonatomic) FLIROneSDKTuningState tuningState;

@property (nonatomic) FLIROneSDKBatteryChargingState batteryChargingState;
@property (nonatomic) NSInteger batteryPercentage;

//@property (nonatomic) FLIROneSDKEmissivity *emissivity;
@property (nonatomic) CGFloat emissivity;
@property (nonatomic) FLIROneSDKImageOptions options;

//@property (nonatomic) BOOL pixelDataExists;
@property (nonatomic) CGPoint pixelOfInterest;
@property (nonatomic) CGPoint coldPixel;
@property (nonatomic) CGFloat pixelTemperature;
@property (nonatomic) CGFloat coldestTemperature;

//@property (nonatomic) BOOL regionDataExists;
@property (nonatomic) CGRect regionOfInterest;
@property (nonatomic) CGFloat regionMinTemperature;
@property (nonatomic) CGFloat regionMaxTemperature;
@property (nonatomic) CGFloat regionAverageTemperature;

@property (nonatomic) BOOL msxDistanceEnabled;

@property (strong, nonatomic) dispatch_queue_t renderQueue;
//@property (strong, nonatomic) NSData *imageData;

@property (nonatomic) NSTimeInterval lastTime;
@property (nonatomic) CGFloat fps;

//capturing video stuff

//if the user is capturing a video or in the process of recording, the camera is "busy", block requests to capture more media
@property (nonatomic) BOOL cameraBusy;

@property (strong, nonatomic) NSURL *lastCapturePath;

//if there is currently a video being recorded
@property (nonatomic) BOOL currentlyRecording;
//is the image finished recording, and currently wrapping up the file write process?
@property (nonatomic) BOOL savingVideo;

@property (nonatomic) NSInteger frameCount;

// arrays for thermal and YCbCr images, to be converted into videos
@property (nonatomic) NSMutableArray * thermalImagesArray;
@property (nonatomic) NSMutableArray * YCbCrImagesArray;

// timestamp to maintain the fps of video made from images array
@property (strong) NSDate *lastDateUpdate;
@property (strong) NSDate *timerLastDateUpdate;


@property CIContext* context;

// textfields for algorithm parameter inputs
@property (weak, nonatomic) IBOutlet UITextField *seedTextfield;
@property (weak, nonatomic) IBOutlet UITextField *upperLimitTextfield;
@property (weak, nonatomic) IBOutlet UITextField *lowerLimitTextfield;

@end

@implementation FLIROneSDKExampleViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // initialise arrays
    self.thermalImagesArray = [[NSMutableArray alloc] init];
    self.YCbCrImagesArray = [[NSMutableArray alloc] init];
    
    self.lastDateUpdate = [NSDate date];
    self.timerLastDateUpdate = [NSDate date];
    
    self.thermalImage = [UIImage imageNamed:@"gray1.jpg"];
    
    [self.timerLabel setText:@"00"];
    
    // Disable UI. The UI is enabled if and only if the session starts running.
    self.cameraButton.enabled = NO;
    self.recordButton.enabled = NO;
    self.stillButton.enabled = NO;
   
    
    self.context = [[CIContext alloc] initWithOptions:nil];
    
    
    [self _start];
    
    
    self.visualJPEGView.hidden = YES;
    self.regionView = [[UIView alloc] initWithFrame:CGRectZero];
    [self.masterImageView addSubview:self.regionView];
    self.regionView.backgroundColor = [UIColor greenColor];
    self.regionView.alpha = 0.5;
    
    self.regionMinLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    [self.masterImageView addSubview:self.regionMinLabel];
    self.regionMaxLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    [self.masterImageView addSubview:self.regionMaxLabel];
    self.regionAverageLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    [self.masterImageView addSubview:self.regionAverageLabel];
    
    self.hottestPoint = [[UIView alloc] initWithFrame:CGRectZero];
    [self.masterImageView addSubview:self.hottestPoint];
    self.hottestPoint.backgroundColor = [UIColor redColor];
    self.hottestPoint.alpha = 0.5;
    self.hottestLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    [self.masterImageView addSubview:self.hottestLabel];
    
    self.coldestPoint = [[UIView alloc] initWithFrame:CGRectZero];
    [self.masterImageView addSubview:self.coldestPoint];
    self.coldestPoint.backgroundColor = [UIColor blueColor];
    self.coldestPoint.alpha = 0.5;
    self.coldestLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    [self.masterImageView addSubview:self.coldestLabel];
    
    //center of screen, half width half height, offset by width/4, height/4
    self.regionOfInterest = CGRectMake(0.25, 0.25, 0.5, 0.5);
    
    //create a queue for rendering
    self.renderQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    //set the options to MSX blended
    self.options = FLIROneSDKImageOptionsThermalRGBA8888Image;
    
    [[FLIROneSDKStreamManager sharedInstance] addDelegate:self];
    
    
    self.lastCapturePath = nil;
    
    self.cameraBusy = NO;
    
    
    
    [self updateUI];
}

NSString* deviceName()
{
    struct utsname systemInfo;
    uname(&systemInfo);
    
    return [NSString stringWithCString:systemInfo.machine
                              encoding:NSUTF8StringEncoding];
}

- (void)_start
{
    
    // select dual camera if suuported , else select normal camera
    
    if([AVCaptureDeviceDiscoverySession class]){
        NSArray *allTypes = @[AVCaptureDeviceTypeBuiltInDualCamera, AVCaptureDeviceTypeBuiltInWideAngleCamera, AVCaptureDeviceTypeBuiltInTelephotoCamera ];
        AVCaptureDeviceDiscoverySession *discoverySession = [AVCaptureDeviceDiscoverySession discoverySessionWithDeviceTypes:allTypes mediaType:AVMediaTypeVideo position:AVCaptureDevicePositionBack];
        
        for(AVCaptureDevice *device in discoverySession.devices) {
            if ([deviceName()  isEqual: @"iPhone9,2"] || [deviceName()  isEqual: @"iPhone9,4"] ||
                [deviceName()  isEqual: @"iPhone10,2"] || [deviceName()  isEqual: @"iPhone10,5"] ||
                [deviceName()  isEqual: @"iPhone10,3"] || [deviceName()  isEqual: @"iPhone10,6"] ||
                [deviceName()  isEqual: @"iPhone11,2"] || [deviceName()  isEqual: @"iPhone11,4"] ||
                [deviceName()  isEqual: @"iPhone11,6"] || [deviceName()  isEqual: @"iPhone11,8"]){
                if(device.deviceType == AVCaptureDeviceTypeBuiltInDualCamera){
                    _videoDevice = device;
                    break;
                }
            } else {
                if(AVCaptureDeviceTypeBuiltInWideAngleCamera){
                    _videoDevice = device;
                    break;
                }
            }
        }
    }
    
    // obtain the preset and validate the preset
    NSString *preset = AVCaptureSessionPreset1280x720;
    if (![_videoDevice supportsAVCaptureSessionPreset:preset])
    {
        NSLog(@"%@", [NSString stringWithFormat:@"Capture session preset not supported by video device: %@", preset]);
        return;
    }
    
    // create the capture session
    _captureSession = [[AVCaptureSession alloc] init];
    _captureSession.sessionPreset = preset;
    
    // obtain device input
    NSError *error = nil;
    AVCaptureDeviceInput *videoDeviceInput = [AVCaptureDeviceInput deviceInputWithDevice:_videoDevice error:&error];
    if (!videoDeviceInput)
    {
        NSLog(@"%@", [NSString stringWithFormat:@"Unable to obtain video device input, error: %@", error]);
        return;
    }
    
    // add output
    
    // CoreImage wants BGRA pixel format
    NSDictionary *outputSettings = @{ (id)kCVPixelBufferPixelFormatTypeKey : [NSNumber numberWithInteger:kCVPixelFormatType_32BGRA]};
    // create and configure video data output
    AVCaptureVideoDataOutput *videoDataOutput = [[AVCaptureVideoDataOutput alloc] init];
    videoDataOutput.videoSettings = outputSettings;
    
    // create the dispatch queue for handling capture session delegate method calls
    _captureSessionQueue = dispatch_queue_create("capture_session_queue", NULL);
    [videoDataOutput setSampleBufferDelegate:self queue:_captureSessionQueue];
    
    videoDataOutput.alwaysDiscardsLateVideoFrames = YES;
    
    if (![_captureSession canAddOutput:videoDataOutput])
    {
        NSLog(@"Cannot add video data output");
        _captureSession = nil;
        return;
    }
    
    AVCaptureConnection *captureConnection = [videoDataOutput connectionWithMediaType:AVMediaTypeVideo];
    if ([captureConnection isVideoOrientationSupported]) {
        captureConnection.videoOrientation = AVCaptureVideoOrientationLandscapeLeft;
    }
    
    AVCaptureDepthDataOutput *depthOutput = [[AVCaptureDepthDataOutput alloc] init];
    [depthOutput setDelegate:self callbackQueue:_captureSessionQueue];
    [depthOutput setFilteringEnabled:true];
    
    AVCaptureConnection *depthConnection = [depthOutput connectionWithMediaType:AVMediaTypeVideo];
    if ([depthConnection isVideoOrientationSupported]) {
        depthConnection.videoOrientation = AVCaptureVideoOrientationPortrait;
    }
    
    CGRect outputRect = CGRectMake(0, 0, 1, 1);
    CGRect videoRect = [videoDataOutput rectForMetadataOutputRectOfInterest:outputRect];
    CGRect depthRect = [depthOutput rectForMetadataOutputRectOfInterest:outputRect];
    
    CGFloat scale = MAX(videoRect.size.width, videoRect.size.height) / MAX(depthRect.size.width, depthRect.size.height);
    
    self.previewView.session = _captureSession;
    
    // begin configure capture session
    [_captureSession beginConfiguration];
    
    // connect the video device input and video data and still image outputs
    [_captureSession addInput:videoDeviceInput];
    [_captureSession addOutput:videoDataOutput];
    [_captureSession addOutput:depthOutput];
    
    [_videoDevice lockForConfiguration:nil];
    
    CMTime frameDuration = [[[[_videoDevice activeDepthDataFormat] videoSupportedFrameRateRanges] firstObject] minFrameDuration];
    _videoDevice.activeVideoMinFrameDuration = frameDuration;
    
    
//    // resolution
//    FaceDetection *faceDetection = [[FaceDetection alloc] init];
//    _videoDevice.activeFormat = [faceDetection highestResolution420FormatFor:_videoDevice][0];
    
    [_videoDevice unlockForConfiguration];
    
    [_captureSession commitConfiguration];
//
//    faceDetection.previewLayer = self.previewView;
//    faceDetection.captureDeviceResolution = [faceDetection highestResolution420FormatFor:_videoDevice][1];
//
//    [faceDetection prepareVisionRequest];
    
    // then start everything
    [_captureSession startRunning];
    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    
    
    
    
   
    
    [[FLIROneSDKStreamManager sharedInstance] setImageOptions:self.options];
    [[FLIROneSDKStreamManager sharedInstance] setFrameDropEnabled:NO];
}

- (IBAction) switchPalette:(UIButton *)button {
    NSInteger paletteIndex = [[[FLIROneSDKPalette palettes] allValues] indexOfObject:self.palette];
    if(paletteIndex >= 0 && paletteIndex < [[[FLIROneSDKPalette palettes] allValues] count]) {
        self.paletteCount = paletteIndex;
    }
    else {
        self.paletteCount = 0;
    }
    self.paletteCount = ((self.paletteCount+1) % [[FLIROneSDKPalette palettes] count]);
    FLIROneSDKPalette *palette = [[[FLIROneSDKPalette palettes] allValues] objectAtIndex:self.paletteCount];

    [[FLIROneSDKStreamManager sharedInstance] setPalette:palette];
}

- (void) updateUI {
    //updates the UI based on the state of the sled
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        // save thermal and YCbCr images in their arrays,
//        if (self.currentlyRecording && (([[NSDate date] timeIntervalSince1970] - self.lastDateUpdate.timeIntervalSince1970) > 0.02)) {
//
//            [self.thermalImagesArray addObject:self.thermalImageView.image];
//            //UIImageWriteToSavedPhotosAlbum(self.visualYCbCrView.image, nil, nil, nil);
//            //UIImage *image = [UIImage imageWithCGImage:self.visualYCbCrView.image.CGImage scale:1.0 orientation:UIImageOrientationRight];
//            //[self.YCbCrImagesArray addObject:image];
//
//            self.lastDateUpdate = [NSDate date];
//        }
        
        
        [self.thermalImageView setImage:self.thermalImage];
        [self.radiometricImageView setImage:self.radiometricImage];
        [self.visualJPEGView setImage:self.visualJPEGImage];
        [self.visualYCbCrView setImage:self.visualYCbCrImage];
        
        if(self.thermalData && self.options & (FLIROneSDKImageOptionsThermalRadiometricKelvinImage | FLIROneSDKImageOptionsThermalRadiometricKelvinImageFloat)) {
            //find hottest point
            //self.hottestPoint.hidden = NO;

            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                @synchronized(self) {
                    [self performTemperatureCalculationsFloat:self.thermalDataIsFloats];
                }
                
                //self.pixelDataExists = true;
                //self.regionDataExists = true;
                dispatch_async(dispatch_get_main_queue(), ^{
                    //update the positions of the hottest, coldest, and temperature region views/labels
                    CGSize imageSize = self.radiometricImageView.frame.size;
                    CGPoint imageOrigin = self.radiometricImageView.frame.origin;
                    
                    CGRect frame;
                    CGFloat size = 30;
                    frame.origin.x = imageOrigin.x + imageSize.width*self.pixelOfInterest.x - size/2.0;
                    frame.origin.y = imageOrigin.y + imageSize.height*self.pixelOfInterest.y - size/2.0;
                    frame.size.width = size;
                    frame.size.height = size;
                    self.hottestPoint.frame = frame;
                    frame.size.width = 100;
                    self.hottestLabel.frame = frame;
                    self.hottestLabel.text = [NSString stringWithFormat:@"%0.2fºC", self.pixelTemperature-273.15];
                    
                    frame = CGRectZero;
                    size = 30;
                    frame.origin.x = imageOrigin.x + imageSize.width * self.coldPixel.x - size/2.0;
                    frame.origin.y = imageOrigin.y + imageSize.height * self.coldPixel.y - size/2.0;
                    frame.size.width = size;
                    frame.size.height = size;
                    self.coldestPoint.frame = frame;
                    frame.size.width = 100;
                    self.coldestLabel.frame = frame;
                    self.coldestLabel.text = [NSString stringWithFormat:@"%0.2fºC", self.coldestTemperature-273.15];
                
                    frame.origin.x = imageOrigin.x + imageSize.width*self.regionOfInterest.origin.x;
                    frame.origin.y = imageOrigin.y + imageSize.height*self.regionOfInterest.origin.y;
                    frame.size.width = imageSize.width*self.regionOfInterest.size.width;
                    frame.size.height = imageSize.height*self.regionOfInterest.size.height;
                    self.regionView.frame = frame;
                    frame.size.width = 100;
                    frame.size.height = 30;
                    self.regionMinLabel.frame = frame;
                    frame.origin.y += 30;
                    self.regionAverageLabel.frame = frame;
                    frame.origin.y += 30;
                    self.regionMaxLabel.frame = frame;
                });
                
                
            });
            
            
        } else {
            //self.pixelDataExists = false;
            //self.regionDataExists = false;
            
            self.hottestLabel.text = @"";
            self.hottestPoint.frame = CGRectZero;
            self.coldestLabel.frame = CGRectZero;
            self.coldestPoint.frame = CGRectZero;
            self.regionMaxLabel.text = @"";
            self.regionMinLabel.text = @"";
            self.regionAverageLabel.text = @"";
            self.regionView.frame = CGRectZero;
            self.regionMaxLabel.frame = CGRectZero;
            self.regionMinLabel.frame = CGRectZero;
            self.regionAverageLabel.frame = CGRectZero;
        }
        
        if(self.palette)
            [self.paletteButton setTitle:[NSString stringWithFormat:@"%@", [self.palette name]] forState:UIControlStateNormal];
        else
            [self.paletteButton setTitle:@"N/A" forState:UIControlStateNormal];
        
        if(self.connected) {
            if(self.isProDongle) {
                [self.connectionLabel setText:@"Gen 3 Pro"];
            }
            else if(self.isGen3Dongle) {
                [self.connectionLabel setText:@"Gen 3 Consumer"];
            }
            else if(self.isDongle) {
                [self.connectionLabel setText:@"Gen 2"];
            }
            else {
                [self.connectionLabel setText:@"Sled"];
            }
            
            [self.capturePhotoButton setEnabled:!self.cameraBusy];
            [self.captureVideoButton setEnabled:(!self.cameraBusy || self.currentlyRecording)];
            [self.spanLockButton setEnabled:YES];
        } else {
            [self.connectionLabel setText:@"Disconnected"];
            [self.capturePhotoButton setEnabled:NO];
            [self.captureVideoButton setEnabled:NO];
            [self.spanLockButton setEnabled:NO];
        }
        
        if([FLIROneSDKStreamManager sharedInstance].spanLockEnabled) {
            [self.spanLockButton setTitle:@"Span Lock: On" forState:UIControlStateNormal];
        }
        else {
            [self.spanLockButton setTitle:@"Span Lock: Off" forState:UIControlStateNormal];
        }
        
        NSString *tuningStateString;
        switch(self.tuningState) {
            case FLIROneSDKTuningStateTuningSuggested:
                tuningStateString = @"Tuning Suggested";
                break;
            case FLIROneSDKTuningStateInProgress:
                tuningStateString = @"Tuning Progress";
                break;
            case FLIROneSDKTuningStateUnknown:
                tuningStateString = @"Tuning Unknown";
                break;
            case FLIROneSDKTuningStateTunedWithClosedShutter:
                tuningStateString = @"Tuned Closed";
                break;
            case FLIROneSDKTuningStateTunedWithOpenedShutter:
                tuningStateString = @"Tuned Open";
                break;
            case FLIROneSDKTuningStateTuningRequired:
                tuningStateString = @"Tuning Required";
                break;
            case FLIROneSDKTuningStateApproximatelyTunedWithOpenedShutter:
                tuningStateString = @"Tuned Approx.";
                break;
        }
        [self.tuningStateLabel setText:[NSString stringWithFormat:@"%@", tuningStateString]];
        
        [self.versionLabel setText:[FLIROneSDK version]];
        
        [self.batteryPercentageLabel setText:[NSString stringWithFormat:@"Battery: %ld%%", (long)self.batteryPercentage]];
        
        
        NSString *chargingState;
        switch(self.batteryChargingState) {
            case FLIROneSDKBatteryChargingStateCharging:
                chargingState = @"Yes";
                break;
            case FLIROneSDKBatteryChargingStateDischarging:
                chargingState = @"No";
                break;
                
            case FLIROneSDKBatteryChargingStateError:
                chargingState = @"Err";
                break;
            case FLIROneSDKBatteryChargingStateInvalid:
                chargingState = @"Invalid";
                break;
            default:
                chargingState = @"N/A";
                break;
        }
        [self.batteryChargingLabel setText:[NSString stringWithFormat:@"Charging: %@", chargingState]];
        
        
        if(self.currentlyRecording) {
            [self.captureVideoButton setTitle:@"Stop Video" forState:UIControlStateNormal];
        } else {
            [self.captureVideoButton setTitle:@"Start Video" forState:UIControlStateNormal];
        }
        
        [self.msxButton setTitle:[NSString stringWithFormat:@"MSX Distance: %@", (self.msxDistanceEnabled ? @"On" : @"Off")] forState:UIControlStateNormal];
        [self.emissivityButton setTitle:[NSString stringWithFormat:@"Emissivity: %0.2f", self.emissivity] forState:UIControlStateNormal];
        
        self.frameCountLabel.text = [NSString stringWithFormat:@"Count: %ld, %f", (long)self.frameCount, self.fps];
        
        [self.editButton setEnabled:(self.lastCapturePath != nil)];
    });
}

- (void) performTemperatureCalculationsFloat:(BOOL)isFloat {
    float temp;
    float hottestTemp;
    float coldestTemp;
    int index = 0;
    int coldIndex = 0;
    
    float minRegion = 0;
    int minRegionIndex = 0;
    float maxRegion = 0;
    int maxRegionIndex = 0;
    unsigned long regionCount = 0;
    double regionSum = 0;
    
    float *tempDataFloat = nullptr;
    uint16_t *tempDataInt = nullptr;
    
    if(isFloat) {
        tempDataFloat = (float *)[self.thermalData bytes];
        temp = tempDataFloat[0];
    }
    else {
        tempDataInt = (uint16_t *)[self.thermalData bytes];
        temp = tempDataInt[0]/100.0;
    }
    
    hottestTemp = temp;
    coldestTemp = temp;
    
    
    for(int i=0;i<self.thermalSize.width*self.thermalSize.height;i++) {
        if(isFloat) {
            temp = tempDataFloat[i];
        }
        else {
            temp = tempDataInt[i]/100.0;
        }
        if(temp > hottestTemp) {
            hottestTemp = temp;
            index = i;
        }
        if(temp < coldestTemp) {
            coldestTemp = temp;
            coldIndex = i;
        }
        CGFloat x = (i % (int)self.thermalSize.width)/self.thermalSize.width;
        CGFloat y = (i / self.thermalSize.width)/self.thermalSize.height;
        
        if(x > self.regionOfInterest.origin.x
           && x < self.regionOfInterest.origin.x + self.regionOfInterest.size.width
           && y > self.regionOfInterest.origin.y
           && y < self.regionOfInterest.origin.y + self.regionOfInterest.size.height) {
            
            if(regionCount == 0) {
                minRegion = temp;
                minRegionIndex = i;
                maxRegion = temp;
                maxRegionIndex = i;
            }
            else {
                if(temp > maxRegion) {
                    maxRegion = temp;
                    maxRegionIndex = i;
                }
                if(temp < minRegion) {
                    minRegion = temp;
                    minRegionIndex = i;
                }
            }
            
            regionCount += 1;
            regionSum += temp;
        }
    }
    double regionAverage = (regionSum/regionCount);
    
    NSInteger column = index % (int)self.thermalSize.width;
    NSInteger row = index / self.thermalSize.width;
    //update the thinger
    self.pixelOfInterest = CGPointMake(column/self.thermalSize.width, row/self.thermalSize.height);
    
    column = coldIndex % (int)self.thermalSize.width;
    row = coldIndex / self.thermalSize.width;
    self.coldPixel = CGPointMake(column/self.thermalSize.width, row/self.thermalSize.height);
    
    self.coldestTemperature = coldestTemp;
    self.pixelTemperature = hottestTemp;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        self.regionMaxLabel.text = [NSString stringWithFormat:@"%0.2fºC", maxRegion-273.15];
        self.regionMinLabel.text = [NSString stringWithFormat:@"%0.2fºC", minRegion-273.15];
        self.regionAverageLabel.text = [NSString stringWithFormat:@"%0.2fºC", regionAverage-273.15];
    });
    
    
}

//events relating to user tapping the image views, switches formats on and off

- (void) disableImageOptions:(FLIROneSDKImageOptions)imageOptions {
    self.options = self.options & ~imageOptions;
}

- (void) enableImageOptions:(FLIROneSDKImageOptions)imageOptions {
    self.options = self.options | imageOptions;
}

- (void) toggleImageOptions:(FLIROneSDKImageOptions)imageOptions {
    self.options = self.options ^ imageOptions;
}

- (void) FLIROneSDKDidConnect {
    self.connected = YES;
    self.isDongle = [[FLIROneSDKStreamManager sharedInstance] isDongle];
    self.isGen3Dongle = [[FLIROneSDKStreamManager sharedInstance] isGen3Dongle];
    self.isProDongle = [[FLIROneSDKStreamManager sharedInstance] isProDongle];
    self.frameCount = 0;
    
    
    [self updateUI];
    
   
}

- (void) FLIROneSDKDidDisconnect {
    self.connected = NO;
    [self updateUI];
}


//callbacks for image data delivered from sled
- (void)FLIROneSDKDelegateManager:(FLIROneSDKDelegateManager *)delegateManager didReceiveFrameWithOptions:(FLIROneSDKImageOptions)options metadata:(FLIROneSDKImageMetadata *)metadata sequenceNumber:(NSInteger)sequenceNumber {
    NSLog(@"received frame: %lu", sequenceNumber);
    
    self.emissivity = metadata.emissivity;
    self.palette = metadata.palette;
    
    self.frameCount += 1;
    
    
    NSTimeInterval now = [[NSDate date] timeIntervalSince1970];
    
    if(self.lastTime > 0) {
        self.fps = 1.0/(now - self.lastTime);
    }
    
    self.lastTime = now;
    
    [self updateUI];
}

- (void)FLIROneSDKDelegateManager:(FLIROneSDKDelegateManager *)delegateManager didReceiveBlendedMSXRGBA8888Image:(NSData *)msxImage imageSize:(CGSize)size sequenceNumber:(NSInteger)sequenceNumber {
    NSLog(@"msx frame: %lu", sequenceNumber);
    if(self.options & FLIROneSDKImageOptionsBlendedMSXRGBA8888Image) {
        NSLog(@"DID RECEIVE didReceiveBlendedMSXRGBA8888Image");
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            UIImage *image = [FLIROneSDKUIImage imageWithFormat:FLIROneSDKImageOptionsBlendedMSXRGBA8888Image andData:msxImage andSize:size];
            if(self.options & FLIROneSDKImageOptionsBlendedMSXRGBA8888Image) {
                self.thermalImage = image;
                [self updateUI];
            }
        });
    }
    else {
        self.thermalImage = nil;
        [self updateUI];
    }
    
    //[self updateUI];
}

- (void)FLIROneSDKDelegateManager:(FLIROneSDKDelegateManager *)delegateManager didReceiveThermalRGBA8888Image:(NSData *)thermalImage imageSize:(CGSize)size sequenceNumber:(NSInteger)sequenceNumber {
    NSLog(@"thermal frame: %lu", sequenceNumber);
    if(self.options & FLIROneSDKImageOptionsThermalRGBA8888Image) {
        NSLog(@"DID RECEIVE didReceiveThermalRGBA8888Image");
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            UIImage *image = [FLIROneSDKUIImage imageWithFormat:FLIROneSDKImageOptionsThermalRGBA8888Image andData:thermalImage andSize:size];
            if(self.options & FLIROneSDKImageOptionsThermalRGBA8888Image) {
                self.thermalImage = image;
                [self updateUI];
            }
        });
    }
    else {
        self.thermalImage = nil;
        [self updateUI];
    }
    
    //[self updateUI];
}

- (void)FLIROneSDKDelegateManager:(FLIROneSDKDelegateManager *)delegateManager didReceiveVisualYCbCr888Image:(NSData *)visualYCbCr888Image imageSize:(CGSize)size sequenceNumber:(NSInteger)sequenceNumber {
    NSLog(@"ycbcr frame: %lu", sequenceNumber);
    NSLog(@"DID RECEIVE didReceiveVisualYCbCr888Image");
    if(self.options & FLIROneSDKImageOptionsVisualYCbCr888Image) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            UIImage *image = [FLIROneSDKUIImage imageWithFormat:FLIROneSDKImageOptionsVisualYCbCr888Image andData:visualYCbCr888Image andSize:size];
            
            if(self.options & FLIROneSDKImageOptionsVisualYCbCr888Image) {
                self.visualYCbCrImage = image;
                [self updateUI];
            }
        });
    }
    else {
        self.visualYCbCrImage = nil;
        [self updateUI];
    }
    
    //[self updateUI];
}

- (void)FLIROneSDKDelegateManager:(FLIROneSDKDelegateManager *)delegateManager didReceiveVisualJPEGImage:(NSData *)visualJPEGImage sequenceNumber:(NSInteger)sequenceNumber {
    NSLog(@"jpeg frame: %lu", sequenceNumber);
    
    if(self.options & FLIROneSDKImageOptionsVisualJPEGImage) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            UIImage *rotatedJPEG = [FLIROneSDKUIImage imageWithFormat:FLIROneSDKImageOptionsVisualJPEGImage andData:visualJPEGImage andSize:CGSizeZero];
            
            UIImage *image = [[UIImage alloc]
                                    initWithCGImage:rotatedJPEG.CGImage
                                    scale:1.0
                                    orientation:UIImageOrientationRight];
            if(self.options & FLIROneSDKImageOptionsVisualJPEGImage) {
                self.visualJPEGImage = image;
                [self updateUI];
            }
        });
    }
    else {
        self.visualJPEGImage = nil;
        [self updateUI];
    }
    
    //[self updateUI];
}

- (void)FLIROneSDKDelegateManager:(FLIROneSDKDelegateManager *)delegateManager didReceiveThermal14BitLinearFluxImage:(NSData *)linearFluxImage imageSize:(CGSize)size sequenceNumber:(NSInteger)sequenceNumber {
    NSLog(@"flux frame: %lu", sequenceNumber);
    
    if(self.options & FLIROneSDKImageOptionsThermalLinearFlux14BitImage) {
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            UIImage *image = [FLIROneSDKUIImage imageWithFormat:FLIROneSDKImageOptionsThermalLinearFlux14BitImage andData:linearFluxImage andSize:size];
            if(self.options & FLIROneSDKImageOptionsThermalLinearFlux14BitImage) {
                self.radiometricImage = image;
                [self updateUI];
            }
        });
    }
    else {
        self.radiometricImage = nil;
        [self updateUI];
    }
}

- (void)FLIROneSDKDelegateManager:(FLIROneSDKDelegateManager *)delegateManager didReceiveRadiometricData:(NSData *)radiometricData imageSize:(CGSize)size sequenceNumber:(NSInteger)sequenceNumber {
    NSLog(@"rad frame: %lu", sequenceNumber);
    
    if(self.options & FLIROneSDKImageOptionsThermalRadiometricKelvinImage) {
        @synchronized(self) {
            self.thermalDataIsFloats = NO;
            self.thermalData = radiometricData;
            self.thermalSize = size;
        }
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            UIImage *image = [FLIROneSDKUIImage imageWithFormat:FLIROneSDKImageOptionsThermalRadiometricKelvinImage andData:radiometricData andSize:size];
            if(self.options & FLIROneSDKImageOptionsThermalRadiometricKelvinImage) {
                self.radiometricImage = image;
                [self updateUI];
            }
        });
    }
    else {
        self.radiometricImage = nil;
        [self updateUI];
    }
    
}

- (void)FLIROneSDKDelegateManager:(FLIROneSDKDelegateManager *)delegateManager didReceiveRadiometricDataFloat:(NSData *)radiometricData imageSize:(CGSize)size sequenceNumber:(NSInteger)sequenceNumber {
    NSLog(@"rad float frame: %lu", sequenceNumber);
    if(self.options & FLIROneSDKImageOptionsThermalRadiometricKelvinImageFloat) {
        @synchronized(self) {
            self.thermalDataIsFloats = YES;
            self.thermalData = radiometricData;
            self.thermalSize = size;
        }
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            UIImage *image = [FLIROneSDKUIImage imageWithFormat:FLIROneSDKImageOptionsThermalRadiometricKelvinImageFloat andData:radiometricData andSize:size];
            if(self.options & FLIROneSDKImageOptionsThermalRadiometricKelvinImageFloat) {
                self.radiometricImage = image;
                [self updateUI];
            }
        });
    }
    else {
        self.radiometricImage = nil;
        [self updateUI];
    }
}

- (void)FLIROneSDKDidRenderToGLKView:(GLKView *)glkView sequenceNumber:(NSInteger)sequenceNumber {
    NSLog(@"gl frame: %lu", sequenceNumber);
}

- (void) FLIROneSDKAutomaticTuningDidChange:(NSNumber *)deviceWillTuneAutomatically{
    [self.autoTuneSwitch setOn:[deviceWillTuneAutomatically boolValue] animated:YES];
}

//callbacks relating to capturing images to library
- (void) FLIROneSDKDidFinishCapturingPhoto:(FLIROneSDKCaptureStatus)captureStatus withFilepath:(NSURL *)filepath {
    self.cameraBusy = NO;
    if(captureStatus == FLIROneSDKCaptureStatusSucceeded) {
        self.lastCapturePath = filepath;
    }
    [self updateUI];
}

//tuning callback
- (void) FLIROneSDKTuningStateDidChange:(FLIROneSDKTuningState)newTuningState {
    self.tuningState = newTuningState;
    [self updateUI];
}

//charging callback
- (void) FLIROneSDKBatteryChargingStateDidChange:(FLIROneSDKBatteryChargingState)state {
    self.batteryChargingState = state;
    [self updateUI];
}

//battery callback
- (void) FLIROneSDKBatteryPercentageDidChange:(NSNumber *)percentage {
    self.batteryPercentage = [percentage integerValue];
    [self updateUI];
}

//enable or disable MSX
- (IBAction) msxButtonPressed:(UIButton *)button {
    self.msxDistanceEnabled = !self.msxDistanceEnabled;
    
    [FLIROneSDKStreamManager sharedInstance].msxDistanceEnabled = YES;
    [FLIROneSDKStreamManager sharedInstance].msxDistance = self.msxDistanceEnabled ? 0 : 1;
    
}

- (IBAction) tuneThermalPressed:(UIButton *)button {
    [[FLIROneSDKStreamManager sharedInstance] performTuning];
}

- (IBAction) autoTuneToggle:(UISwitch *)tuneSwitch{
    [[FLIROneSDKStreamManager sharedInstance] setAutomaticTuning:[tuneSwitch isOn]];
}

//switch emissivity value to one of 5 values
- (IBAction) emissivityPressed:(UIButton *)button {
    CGFloat customValue = 0.5;
    
    if(fabs(self.emissivity - FLIROneSDKEmissivityGlossy) < 0.01) {
        self.emissivity = FLIROneSDKEmissivitySemiGlossy;
    } else if(fabs(self.emissivity - FLIROneSDKEmissivitySemiGlossy) < 0.01) {
        self.emissivity = FLIROneSDKEmissivitySemiMatte;
    } else if(fabs(self.emissivity - FLIROneSDKEmissivitySemiMatte) < 0.01) {
        self.emissivity = FLIROneSDKEmissivityMatte;
    } else if(fabs(self.emissivity - FLIROneSDKEmissivityMatte) < 0.01) {
        self.emissivity = customValue;
    } else if(fabs(self.emissivity - customValue) < 0.01) {
        self.emissivity = FLIROneSDKEmissivityGlossy;
    } else {
        self.emissivity = customValue;
    }
    [[FLIROneSDKStreamManager sharedInstance] setEmissivity:self.emissivity];
}
- (IBAction)vividIRQualityButtonPressed:(id)sender {
    
    switch([[FLIROneSDKStreamManager sharedInstance] vividIRQuality]) {
        case FLIROneSDKVividIRQualityNone:
            [FLIROneSDKStreamManager sharedInstance].vividIRQuality = FLIROneSDKVividIRQualityLow;
            [self.vividIRButton setTitle:@"Vivid IR: Low" forState:UIControlStateNormal];
            break;
        case FLIROneSDKVividIRQualityLow:
            [FLIROneSDKStreamManager sharedInstance].vividIRQuality = FLIROneSDKVividIRQualityHigh;
            [self.vividIRButton setTitle:@"Vivid IR: High" forState:UIControlStateNormal];
            break;
        case FLIROneSDKVividIRQualityHigh:
            [FLIROneSDKStreamManager sharedInstance].vividIRQuality = FLIROneSDKVividIRQualityNone;
            [self.vividIRButton setTitle:@"Vivid IR: None" forState:UIControlStateNormal];
            break;
    }
}

- (IBAction)capturePhoto:(id)sender {
    self.cameraBusy = YES;
    [self updateUI];


    NSURL *filepath = [FLIROneSDKLibraryManager libraryFilepathForCurrentTimestampWithExtension:@"jpg"];

    [[FLIROneSDKStreamManager sharedInstance] capturePhotoWithFilepath:filepath];

}

- (IBAction) captureVideo:(id)sender {
    @synchronized([FLIROneSDKExampleViewController class]) {
        self.cameraBusy = YES;
        if(self.currentlyRecording) {
            //stop recording
            [[FLIROneSDKStreamManager sharedInstance] stopRecordingVideo];
        } else {
            NSURL *filepath = [FLIROneSDKLibraryManager libraryFilepathForCurrentTimestampWithExtension:@"mov"];
            [[FLIROneSDKStreamManager sharedInstance] startRecordingVideoWithFilepath:filepath withVideoRendererDelegate:self];
        }
        
        [self updateUI];
    }
}

- (IBAction)connectSimulator:(UIButton *)sender {
    [[FLIROneSDKSimulation sharedInstance] connectWithFrameBundleName:@"sampleframes_hq" withBatteryChargePercentage:@42];
    
}

- (IBAction)disconnectSimulator:(UIButton *)sender {
    [[FLIROneSDKSimulation sharedInstance] disconnect];
}
- (IBAction)spanLockButtonPressed:(id)sender {
    [FLIROneSDKStreamManager sharedInstance].spanLockEnabled = ![FLIROneSDKStreamManager sharedInstance].spanLockEnabled;
}


- (IBAction)algorithmSwitchToggled:(UISwitch *)sender {
    if(self.thresholdSwitch.isOn) {
        [self.thresholdSwitch setOn:false];
        [self.thresholdSwitch setUserInteractionEnabled:false];
    }
    else {
        [self.thresholdSwitch setUserInteractionEnabled:true];
    }
}


//callbacks for video recording
- (void) FLIROneSDKDidStartRecordingVideo:(FLIROneSDKCaptureStatus)captureStartStatus {
    self.timerLastDateUpdate = [NSDate date];
    self.lastDateUpdate = [NSDate date];
    if(captureStartStatus == FLIROneSDKCaptureStatusSucceeded) {
        self.currentlyRecording = YES;
        self.cameraBusy = YES;
    } else {
        self.cameraBusy = NO;
        self.currentlyRecording = NO;
    }
    
    [self updateUI];
}

- (void) FLIROneSDKDidStopRecordingVideo:(FLIROneSDKCaptureStatus)captureStopStatus {
    self.currentlyRecording = NO;
    
    if(captureStopStatus == FLIROneSDKCaptureStatusFailedWithUnknownError) {
        self.cameraBusy = NO;
    } else {
        self.cameraBusy = YES;
    }
    
    [self updateUI];
}

- (void) FLIROneSDKDidFinishWritingVideo:(FLIROneSDKCaptureStatus)captureWriteStatus withFilepath:(NSString *)videoFilepath {
    self.cameraBusy = NO;
    self.currentlyRecording = NO;
    
    [self updateUI];
    
    //[self saveMovieToLibrary: self.thermalImagesArray];
    //[self saveMovieToLibrary: self.YCbCrImagesArray];
}

//grab any valid image delivered from the sled
- (UIImage *)currentImage {
    
    if(self.thermalImage) {
        return self.thermalImage;
    }
    if(self.visualYCbCrImage) {
        return self.visualYCbCrImage;
    }
    if(self.radiometricImage) {
        return self.radiometricImage;
    }
    if(self.visualJPEGImage) {
        return self.visualJPEGImage;
    }
    return nil;
}


//callback for rendering video in arbitrary video format
- (UIImage *)imageForFrameAtTimestamp:(CMTime)timestamp {
    NSLog(@"size: %@", NSStringFromCGSize(self.currentImage.size));
    NSLog(@"%d, %lld", timestamp.timescale, timestamp.value);
    NSTimeInterval uptime = [[NSProcessInfo processInfo] systemUptime];
    NSLog(@"uptime: %f", uptime);
    return [self currentImage];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"editPhotoSegue"]) {
        FLIROneSDKEditorViewController *editorVC = (FLIROneSDKEditorViewController *)segue.destinationViewController;
        
        editorVC.filepath = self.lastCapturePath;
    }
}


























#pragma mark Orientation

- (BOOL)shouldAutorotate
{
    // Disable autorotation of the interface when recording is in progress.
    return ! self.movieFileOutput.isRecording;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAll;
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    
    // Note that the app delegate controls the device orientation notifications required to use the device orientation.
    UIDeviceOrientation deviceOrientation = [UIDevice currentDevice].orientation;
    if ( UIDeviceOrientationIsPortrait( deviceOrientation ) || UIDeviceOrientationIsLandscape( deviceOrientation ) ) {
        AVCaptureVideoPreviewLayer *previewLayer = (AVCaptureVideoPreviewLayer *)self.previewView;
        previewLayer.connection.videoOrientation = (AVCaptureVideoOrientation)deviceOrientation;
    }
}

#pragma mark KVO and Notifications

- (void)addObservers
{
    [self.session addObserver:self forKeyPath:@"running" options:NSKeyValueObservingOptionNew context:SessionRunningContext];
    [self.stillImageOutput addObserver:self forKeyPath:@"capturingStillImage" options:NSKeyValueObservingOptionNew context:CapturingStillImageContext];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(subjectAreaDidChange:) name:AVCaptureDeviceSubjectAreaDidChangeNotification object:self.videoDeviceInput.device];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sessionRuntimeError:) name:AVCaptureSessionRuntimeErrorNotification object:self.session];
    // A session can only run when the app is full screen. It will be interrupted in a multi-app layout, introduced in iOS 9,
    // see also the documentation of AVCaptureSessionInterruptionReason. Add observers to handle these session interruptions
    // and show a preview is paused message. See the documentation of AVCaptureSessionWasInterruptedNotification for other
    // interruption reasons.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sessionWasInterrupted:) name:AVCaptureSessionWasInterruptedNotification object:self.session];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sessionInterruptionEnded:) name:AVCaptureSessionInterruptionEndedNotification object:self.session];
}

- (void)removeObservers
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [self.session removeObserver:self forKeyPath:@"running" context:SessionRunningContext];
    [self.stillImageOutput removeObserver:self forKeyPath:@"capturingStillImage" context:CapturingStillImageContext];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ( context == CapturingStillImageContext ) {
        BOOL isCapturingStillImage = [change[NSKeyValueChangeNewKey] boolValue];
        
        if ( isCapturingStillImage ) {
            dispatch_async( dispatch_get_main_queue(), ^{
                self.previewView.opacity = 0.0;
                [UIView animateWithDuration:0.25 animations:^{
                    self.previewView.opacity = 1.0;
                }];
            } );
        }
    }
    else if ( context == SessionRunningContext ) {
        BOOL isSessionRunning = [change[NSKeyValueChangeNewKey] boolValue];
        
        dispatch_async( dispatch_get_main_queue(), ^{
            // Only enable the ability to change camera if the device has more than one camera.
            self.cameraButton.enabled = isSessionRunning && ( [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo].count > 1 );
            self.recordButton.enabled = isSessionRunning;
            self.stillButton.enabled = isSessionRunning;
        } );
    }
    else {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

- (void)subjectAreaDidChange:(NSNotification *)notification
{
    CGPoint devicePoint = CGPointMake( 0.5, 0.5 );
    [self focusWithMode:AVCaptureFocusModeContinuousAutoFocus exposeWithMode:AVCaptureExposureModeContinuousAutoExposure atDevicePoint:devicePoint monitorSubjectAreaChange:NO];
}

- (void)sessionRuntimeError:(NSNotification *)notification
{
    NSError *error = notification.userInfo[AVCaptureSessionErrorKey];
    NSLog( @"Capture session runtime error: %@", error );
    
    // Automatically try to restart the session running if media services were reset and the last start running succeeded.
    // Otherwise, enable the user to try to resume the session running.
    if ( error.code == AVErrorMediaServicesWereReset ) {
        dispatch_async( self.sessionQueue, ^{
            if ( self.isSessionRunning ) {
                [self.session startRunning];
                self.sessionRunning = self.session.isRunning;
            }
            else {
                dispatch_async( dispatch_get_main_queue(), ^{
                    self.resumeButton.hidden = NO;
                } );
            }
        } );
    }
    else {
        self.resumeButton.hidden = NO;
    }
}

- (void)sessionWasInterrupted:(NSNotification *)notification
{
    // In some scenarios we want to enable the user to resume the session running.
    // For example, if music playback is initiated via control center while using AVCam,
    // then the user can let AVCam resume the session running, which will stop music playback.
    // Note that stopping music playback in control center will not automatically resume the session running.
    // Also note that it is not always possible to resume, see -[resumeInterruptedSession:].
    BOOL showResumeButton = NO;
    
    // In iOS 9 and later, the userInfo dictionary contains information on why the session was interrupted.
    /*if ( &AVCaptureSessionInterruptionReasonKey ) {
        AVCaptureSessionInterruptionReason reason = [notification.userInfo[AVCaptureSessionInterruptionReasonKey] integerValue];
        NSLog( @"Capture session was interrupted with reason %ld", (long)reason );
        
        if ( reason == AVCaptureSessionInterruptionReasonAudioDeviceInUseByAnotherClient ||
            reason == AVCaptureSessionInterruptionReasonVideoDeviceInUseByAnotherClient ) {
            showResumeButton = YES;
        }
        else if ( reason == AVCaptureSessionInterruptionReasonVideoDeviceNotAvailableWithMultipleForegroundApps ) {
            // Simply fade-in a label to inform the user that the camera is unavailable.
            self.cameraUnavailableLabel.hidden = NO;
            self.cameraUnavailableLabel.alpha = 0.0;
            [UIView animateWithDuration:0.25 animations:^{
                self.cameraUnavailableLabel.alpha = 1.0;
            }];
        }
    }
    else {
        NSLog( @"Capture session was interrupted" );
        showResumeButton = ( [UIApplication sharedApplication].applicationState == UIApplicationStateInactive );
    }*/
    
    if ( showResumeButton ) {
        // Simply fade-in a button to enable the user to try to resume the session running.
        self.resumeButton.hidden = NO;
        self.resumeButton.alpha = 0.0;
        [UIView animateWithDuration:0.25 animations:^{
            self.resumeButton.alpha = 1.0;
        }];
    }
}

- (void)sessionInterruptionEnded:(NSNotification *)notification
{
    NSLog( @"Capture session interruption ended" );
    
    if ( ! self.resumeButton.hidden ) {
        [UIView animateWithDuration:0.25 animations:^{
            self.resumeButton.alpha = 0.0;
        } completion:^( BOOL finished ) {
            self.resumeButton.hidden = YES;
        }];
    }
    if ( ! self.cameraUnavailableLabel.hidden ) {
        [UIView animateWithDuration:0.25 animations:^{
            self.cameraUnavailableLabel.alpha = 0.0;
        } completion:^( BOOL finished ) {
            self.cameraUnavailableLabel.hidden = YES;
        }];
    }
}

#pragma mark Actions

- (IBAction)resumeInterruptedSession:(id)sender
{
    dispatch_async( self.sessionQueue, ^{
        // The session might fail to start running, e.g., if a phone or FaceTime call is still using audio or video.
        // A failure to start the session running will be communicated via a session runtime error notification.
        // To avoid repeatedly failing to start the session running, we only try to restart the session running in the
        // session runtime error handler if we aren't trying to resume the session running.
        [self.session startRunning];
        self.sessionRunning = self.session.isRunning;
        if ( ! self.session.isRunning ) {
            dispatch_async( dispatch_get_main_queue(), ^{
                NSString *message = NSLocalizedString( @"Unable to resume", @"Alert message when unable to resume the session running" );
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"AVCam" message:message preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString( @"OK", @"Alert OK button" ) style:UIAlertActionStyleCancel handler:nil];
                [alertController addAction:cancelAction];
                [self presentViewController:alertController animated:YES completion:nil];
            } );
        }
        else {
            dispatch_async( dispatch_get_main_queue(), ^{
                self.resumeButton.hidden = YES;
            } );
        }
    } );
}



- (IBAction)snapStillImage:(id)sender
{
    /*dispatch_async( self.sessionQueue, ^{
        AVCaptureConnection *connection = [self.stillImageOutput connectionWithMediaType:AVMediaTypeVideo];
        AVCaptureVideoPreviewLayer *previewLayer = (AVCaptureVideoPreviewLayer *)self.previewView.layer;
        
        // Update the orientation on the still image output video connection before capturing.
        connection.videoOrientation = AVCaptureVideoOrientationLandscapeRight;//previewLayer.connection.;
        
        // Flash set to Auto for Still Capture.
        [FLIROneSDKExampleViewController setFlashMode:AVCaptureFlashModeAuto forDevice:self.videoDeviceInput.device];
        
        // Capture a still image.
        [self.stillImageOutput captureStillImageAsynchronouslyFromConnection:connection completionHandler:^( CMSampleBufferRef imageDataSampleBuffer, NSError *error ) {
            if ( imageDataSampleBuffer ) {
                // The sample buffer is not retained. Create image data before saving the still image to the photo library asynchronously.
                NSData *imageData = [AVCaptureStillImageOutput jpegStillImageNSDataRepresentation:imageDataSampleBuffer];
                
                //self.visualYCbCrImage= [UIImage imageNamed:@"rgb1.jpg"];
                //NSData *imageData2 = UIImagePNGRepresentation(self.visualYCbCrImage);
                
     
                
                
                self.visualYCbCrImage = [UIImage imageWithData:imageData];
                [self.visualYCbCrView setImage:self.visualYCbCrImage];
                
                self.visualJPEGImage = self.thermalImage;
                
    
                [self.visualYCbCrView setImage:self.visualYCbCrImage];
                
    
                
                DangerObjectsDetector *dangerObjectsDetector = [[DangerObjectsDetector alloc] init];
                UIImage *result = [dangerObjectsDetector detectObjectsInRGBImage:self.visualYCbCrImage inGrayImage:self.thermalImage] ;
                
                
                self.visualYCbCrImage = result;
                [self.visualYCbCrView setImage:self.visualYCbCrImage];
                //self.visualYCbCrImage = result;
                
            }
            else {
                NSLog( @"Could not capture still image: %@", error );
            }
        }];
    } );*/
}


- (IBAction)focusAndExposeTap:(UIGestureRecognizer *)gestureRecognizer
{
    CGPoint devicePoint = [(AVCaptureVideoPreviewLayer *)self.previewView captureDevicePointOfInterestForPoint:[gestureRecognizer locationInView:gestureRecognizer.view]];
    [self focusWithMode:AVCaptureFocusModeAutoFocus exposeWithMode:AVCaptureExposureModeAutoExpose atDevicePoint:devicePoint monitorSubjectAreaChange:YES];
}

#pragma mark File Output Recording Delegate


- (void)captureOutput:(AVCaptureOutput *)captureOutput
didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer
       fromConnection:(AVCaptureConnection *)connection {
    
    CGImageRef cgImage = [self imageFromSampleBuffer:sampleBuffer];
    UIImage *uiImage = [[UIImage alloc] initWithCGImage:cgImage];
    //UIImage *newImage = [self scaleAndRotateImage:uiImage];
    ImageRotation *imageRotation = [[ImageRotation alloc] init];
    UIImage *newImage = [imageRotation rotateWithImage:uiImage radians:M_PI/2];
    //UIImage *image = [UIImage imageWithCGImage:cgImage scale:1.0 orientation:UIImageOrientationRight];
    
    dispatch_sync( dispatch_get_main_queue(), ^{
        
        DangerObjectsDetector *dangerObjectsDetector = [[DangerObjectsDetector alloc] init];
        
        UIImage *result = [dangerObjectsDetector detectObjectsInRGBImage:newImage inGrayImage:self.thermalImage withResolutionType:4];
        
        
        
        //    self.visualYCbCrImage = [[UIImage alloc]
        //                             initWithCGImage:result.CGImage
        //                             scale:1.0
        //                             orientation:UIImageOrientationRight];
        self.visualYCbCrImage = result;
        
        if (self.currentlyRecording && (([[NSDate date] timeIntervalSince1970] - self.lastDateUpdate.timeIntervalSince1970) > 5)) {

            UIImage *thermal = self.thermalImageView.image;

            self.lastDateUpdate = [NSDate date];

            [self addImageToCameraRoll:newImage nameOfAlbum:@"RGB_Images"];
            [self addImageToCameraRoll:thermal nameOfAlbum:@"Thermal_Images"];
            //[self addImageToCameraRoll:result nameOfAlbum:@"Output_Images"];

        }

        if (self.currentlyRecording && (([[NSDate date] timeIntervalSince1970] - self.timerLastDateUpdate.timeIntervalSince1970) > 1)) {
            int time = [[NSDate date] timeIntervalSince1970] - self.timerLastDateUpdate.timeIntervalSince1970;
            NSString* str = [NSString stringWithFormat:@"%d", time];
            [self.timerLabel setText: str];

            NSInteger intValue = [self.timerLabel.text integerValue];
            if(intValue >= 5) {
                self.timerLastDateUpdate = [NSDate date];
            }
        }
        
        [self.visualYCbCrView setImage:self.visualYCbCrImage];
    });

    CGImageRelease(cgImage);
}

- (void)depthDataOutput:(AVCaptureDepthDataOutput *)output
     didOutputDepthData:(AVDepthData *)depthData
              timestamp:(CMTime)timestamp
             connection:(AVCaptureConnection *)connection {
    
//    AVDepthData *convertedDepth;
//
//    if([depthData depthDataType] != kCVPixelFormatType_DisparityFloat32) {
//       // convertedDepth = depthData.converting(toDepthDataType: kCVPixelFormatType_DisparityFloat32)
//        convertedDepth = [depthData depthDataByConvertingToDepthDataType: kCVPixelFormatType_DisparityFloat32];
//    } else {
//        convertedDepth = depthData;
//    }
//
//    CVPixelBufferRef pixelBuffer = [convertedDepth depthDataMap];
//    CustomBuffer *bf = [[CustomBuffer alloc] init];
//    pixelBuffer = [bf clamp:pixelBuffer];
//
//    CIImage *depthMap = [[CIImage alloc] initWithCVPixelBuffer:pixelBuffer];
//    CGImageRef cgImage = [self.context createCGImage:depthMap fromRect:[depthMap extent]];
//    UIImage *image = [UIImage imageWithCGImage:cgImage scale:1.0 orientation:UIImageOrientationRight];
//    //UIImage *displayImage = [[UIImage alloc] initWithCIImage:depthMap];
//    dispatch_sync( dispatch_get_main_queue(), ^{
//        //[self.depthMaskView setImage:image];
//    });
//    CGImageRelease(cgImage);
}

- (CGImageRef) imageFromSampleBuffer:(CMSampleBufferRef) sampleBuffer
{
    CVImageBufferRef imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
    CVPixelBufferLockBaseAddress(imageBuffer,0);
    uint8_t *baseAddress = (uint8_t *)CVPixelBufferGetBaseAddressOfPlane(imageBuffer, 0);
    size_t bytesPerRow = CVPixelBufferGetBytesPerRow(imageBuffer);
    size_t width = CVPixelBufferGetWidth(imageBuffer);
    size_t height = CVPixelBufferGetHeight(imageBuffer);
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    
    CGContextRef newContext = CGBitmapContextCreate(baseAddress, width, height, 8, bytesPerRow, colorSpace, kCGBitmapByteOrder32Little | kCGImageAlphaPremultipliedFirst);
    CGImageRef newImage = CGBitmapContextCreateImage(newContext);
    CGContextRelease(newContext);
    
    CGColorSpaceRelease(colorSpace);
    CVPixelBufferUnlockBaseAddress(imageBuffer,0);
    
    return newImage;
}

/*
- (void)captureOutput:(AVCaptureFileOutput *)captureOutput didStartRecordingToOutputFileAtURL:(NSURL *)fileURL fromConnections:(NSArray *)connections
{
    // Enable the Record button to let the user stop the recording.
    dispatch_async( dispatch_get_main_queue(), ^{
        self.recordButton.enabled = YES;
        [self.recordButton setTitle:NSLocalizedString( @"Stop", @"Recording button stop title") forState:UIControlStateNormal];
    });
}

- (void)captureOutput:(AVCaptureFileOutput *)captureOutput didFinishRecordingToOutputFileAtURL:(NSURL *)outputFileURL fromConnections:(NSArray *)connections error:(NSError *)error
{
    // Note that currentBackgroundRecordingID is used to end the background task associated with this recording.
    // This allows a new recording to be started, associated with a new UIBackgroundTaskIdentifier, once the movie file output's isRecording property
    // is back to NO — which happens sometime after this method returns.
    // Note: Since we use a unique file path for each recording, a new recording will not overwrite a recording currently being saved.
    UIBackgroundTaskIdentifier currentBackgroundRecordingID = self.backgroundRecordingID;
    self.backgroundRecordingID = UIBackgroundTaskInvalid;
    
    dispatch_block_t cleanup = ^{
        [[NSFileManager defaultManager] removeItemAtURL:outputFileURL error:nil];
        if ( currentBackgroundRecordingID != UIBackgroundTaskInvalid ) {
            [[UIApplication sharedApplication] endBackgroundTask:currentBackgroundRecordingID];
        }
    };
    
    BOOL success = YES;
    
    if ( error ) {
        NSLog( @"Movie file finishing error: %@", error );
        success = [error.userInfo[AVErrorRecordingSuccessfullyFinishedKey] boolValue];
    }
    if ( success ) {
        // Check authorization status.
 
    }
    else {
        cleanup();
    }
    
    // Enable the Camera and Record buttons to let the user switch camera and start another recording.
    dispatch_async( dispatch_get_main_queue(), ^{
        // Only enable the ability to change camera if the device has more than one camera.
        self.cameraButton.enabled = ( [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo].count > 1 );
        self.recordButton.enabled = YES;
        [self.recordButton setTitle:NSLocalizedString( @"Record", @"Recording button record title" ) forState:UIControlStateNormal];
    });
}*/

#pragma mark Device Configuration

- (void)focusWithMode:(AVCaptureFocusMode)focusMode exposeWithMode:(AVCaptureExposureMode)exposureMode atDevicePoint:(CGPoint)point monitorSubjectAreaChange:(BOOL)monitorSubjectAreaChange
{
    dispatch_async( self.sessionQueue, ^{
        AVCaptureDevice *device = self.videoDeviceInput.device;
        NSError *error = nil;
        if ( [device lockForConfiguration:&error] ) {
            // Setting (focus/exposure)PointOfInterest alone does not initiate a (focus/exposure) operation.
            // Call -set(Focus/Exposure)Mode: to apply the new point of interest.
            if ( device.isFocusPointOfInterestSupported && [device isFocusModeSupported:focusMode] ) {
                device.focusPointOfInterest = point;
                device.focusMode = focusMode;
            }
            
            if ( device.isExposurePointOfInterestSupported && [device isExposureModeSupported:exposureMode] ) {
                device.exposurePointOfInterest = point;
                device.exposureMode = exposureMode;
            }
            
            device.subjectAreaChangeMonitoringEnabled = monitorSubjectAreaChange;
            [device unlockForConfiguration];
        }
        else {
            NSLog( @"Could not lock device for configuration: %@", error );
        }
    } );
}

+ (void)setFlashMode:(AVCaptureFlashMode)flashMode forDevice:(AVCaptureDevice *)device
{
    if ( device.hasFlash && [device isFlashModeSupported:flashMode] ) {
        NSError *error = nil;
        if ( [device lockForConfiguration:&error] ) {
            device.flashMode = flashMode;
            [device unlockForConfiguration];
        }
        else {
            NSLog( @"Could not lock device for configuration: %@", error );
        }
    }
}

+ (AVCaptureDevice *)deviceWithMediaType:(NSString *)mediaType preferringPosition:(AVCaptureDevicePosition)position
{
    NSArray *devices = [AVCaptureDevice devicesWithMediaType:mediaType];
    AVCaptureDevice *captureDevice = devices.firstObject;
    
    for ( AVCaptureDevice *device in devices ) {
        if ( device.position == position ) {
            captureDevice = device;
            break;
        }
    }
    
    return captureDevice;
}

// save photos

- (void)addImageToCameraRoll:(UIImage *)image nameOfAlbum:(NSString *)albumName {
   // NSString *albumName = @"RGBImages";

    void (^saveBlock)(PHAssetCollection *assetCollection) = ^void(PHAssetCollection *assetCollection) {
        [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
            PHAssetChangeRequest *assetChangeRequest = [PHAssetChangeRequest creationRequestForAssetFromImage:image];
            PHAssetCollectionChangeRequest *assetCollectionChangeRequest = [PHAssetCollectionChangeRequest changeRequestForAssetCollection:assetCollection];
            [assetCollectionChangeRequest addAssets:@[[assetChangeRequest placeholderForCreatedAsset]]];

        } completionHandler:^(BOOL success, NSError *error) {
            if (!success) {
                NSLog(@"Error creating asset: %@", error);
            }
        }];
    };

    PHFetchOptions *fetchOptions = [[PHFetchOptions alloc] init];
    fetchOptions.predicate = [NSPredicate predicateWithFormat:@"localizedTitle = %@", albumName];
    PHFetchResult *fetchResult = [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeAlbum subtype:PHAssetCollectionSubtypeAny options:fetchOptions];
    if (fetchResult.count > 0) {
        saveBlock(fetchResult.firstObject);
    } else {
        __block PHObjectPlaceholder *albumPlaceholder;
        [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
            PHAssetCollectionChangeRequest *changeRequest = [PHAssetCollectionChangeRequest creationRequestForAssetCollectionWithTitle:albumName];
            albumPlaceholder = changeRequest.placeholderForCreatedAssetCollection;

        } completionHandler:^(BOOL success, NSError *error) {
            if (success) {
                PHFetchResult *fetchResult = [PHAssetCollection fetchAssetCollectionsWithLocalIdentifiers:@[albumPlaceholder.localIdentifier] options:nil];
                if (fetchResult.count > 0) {
                    saveBlock(fetchResult.firstObject);
                }
            } else {
                NSLog(@"Error creating album: %@", error);
            }
        }];
    }
}

@end
